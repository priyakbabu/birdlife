import { combineReducers } from 'redux';
import MenuItemReducer from './reducer-menu-items';
import BirdListReducer from './reducer-bird-list';
import BirdDetailReducer from './reducer-bird-detail';
import SendEmailReducer from './reducer-sendEmail';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  menuItems: MenuItemReducer,
  birdList: BirdListReducer,
  selectedBirdDetails: BirdDetailReducer,
  sendEmail: SendEmailReducer,
  form: formReducer
});

export default rootReducer;
