export default function () {
    return [
        {
            id: 'img1',
            title: 'Top Oriole',
            description: 'Orioles can be attracted with several types of food: nectar, jelly, orange halves, and mealworms. Oriole Feeders are designed to feed one or a combination of these food items. Nectar for orioles can be made the same as you would for hummingbirds. Mix 1 part sugar to 4 parts water.'
        },
        {
            id: 'img2',
            title: 'Kingfisher',
            description: 'Kingfishers feed on a wide variety of prey. They are most famous for hunting and eating fish, and some species do specialise in catching fish, but other species take crustaceans, frogs and other amphibians, annelid worms, molluscs, insects, spiders, centipedes, reptiles (including snakes), and even birds and mammals.'
        },
        {
            id: 'img3',
            title: 'Piping Plover',
            description: 'Piping plovers eat freshwater and marine invertebrates that have been washed up on shore, as well as other invertebrates. This may include marine worms, insects (fly larvae and beetles), crustaceans, mollusks and other small marine animals and their eggs.'
        },
        {
            id: 'img4',
            title: 'Eagle',
            description: 'Eagle prey items include waterfowl and small mammals like squirrels, prairie dogs, raccoons and rabbits. Bald eagles are opportunistic predators meaning that in addition to hunting for live prey, they will steal from other animals (primarily from other eagles or smaller fish eating birds) or scavenge on carrion.'
        }
    ]
}
