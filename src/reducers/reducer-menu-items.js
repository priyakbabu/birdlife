export default function () {
  return [
    {
      title: 'Home',
      path: ''
    },
    {
      title: 'Gallery',
      path: 'gallery'
    },
    {
      title: 'Contact Us',
      path: 'contact-us'
    },
    {
      title: 'About Us',
      path: 'about-us'
    }
  ]
}