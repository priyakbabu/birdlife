export default function (state = null, action) {
    switch (action.type) {
        case 'SEND_EMAIL':
            return action.payload;
        case 'RESET':
            return {};
    }
    return state;
}