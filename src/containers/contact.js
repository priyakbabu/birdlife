import React, { Component } from 'react';
import { Field, reduxForm, handleSubmit, reset } from 'redux-form';
import { sendMail, resetState } from '../../src/actions/sendEmailAction';
import { connect } from 'react-redux';
import { withAlert } from 'react-alert';

export const TextField = field => {
    const { meta: { touched, error } } = field;
    const className = `form-group  ${touched && error ? "error-message" : ""}`;
    return (
        <div className={className}>
            <input className="form-control" type="text" placeholder={field.placeholder} {...field.input} />
            <div className="text-help">
                {touched ? error : ""}
            </div>
        </div>
    )
};

export const TextAreaField = field => {
    const { meta: { touched, error } } = field;
    const className = `form-group  ${touched && error ? "error-message" : ""}`;
    return (
        <div className={className}>
            <textarea className="form-control contact-text"
                placeholder={field.placeholder}
                {...field.input} />
            <div className="text-help">
                {touched ? error : ""}
            </div>
        </div>
    )
};

export class Contact extends Component {
    constructor(props) {
        super(props);
    }

    submitHandle(values) {
        this.props.sendMail(values);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.sendEmail && nextProps.sendEmail.request) {
            if (nextProps.sendEmail.request.status === 200) {
                this.props.alert.success('Thank you for contacting us!!');
            }
            else {
                this.props.alert.error('Please try again later.');
            }
            this.props.resetState();
        }
    }


    render() {
        const { handleSubmit } = this.props;
        return (
            <div className="container app-contact-us">
                <h3> Contact Us </h3>
                <form onSubmit={handleSubmit(this.submitHandle.bind(this))} id="information-form">
                    <div className="row">
                        <div className="form-details col-sm-6">
                            <Field name="name" placeholder="Enter your name" component={TextField} />
                            <Field name="email" placeholder="Enter your e-mail address" component={TextField} />
                            <Field name="website" placeholder="Enter your website" component={TextField} /><br />
                        </div>
                        <div className="col-sm-6">
                            <Field name="message" placeholder="Enter your message" component={TextAreaField} />
                        </div> 
                        <div className="col-sm-12">
                            <button type="submit" className="contact-button-submit btn btn-primary">Submit</button>
                        </div>
                        <br />
                    </div>
                </form>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};
    if (!values.name) {
        errors.name = "Enter a name";
    }
    if (!values.email) {
        errors.email = "Enter a valid Email";
    }
    if (!values.website) {
        errors.website = "Enter a website";
    }
    if (!values.message) {
        errors.message = "Enter a message";
    }
    return errors;
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('contactForm'));


function mapStateToProps(state) {
    return { sendEmail: state.sendEmail };
}

export default withAlert(reduxForm({
    validate,
    form: 'contactForm',
    onSubmitSuccess: afterSubmit
})(connect(mapStateToProps, { sendMail, resetState })(Contact)));