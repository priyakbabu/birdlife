import React, { Component } from 'react';
import { connect } from 'react-redux';
import bindActionCreators from 'redux';
import BirdDetailAction from '../../src/actions'

export const BirdDetail = ({ bird }) => {
  if (!bird) {
    return (<div> Loading !!! </div>);
  }
  return (
    <div>
      <h3>{bird.title} </h3>
      <img src={`../src/resources/images/${bird.title}.jpg`} alt={bird.title} />
      <h5> Feeding Habits </h5>
      <p> {bird.description} </p>
    </div>
  );
}

class Gallery extends Component {
  renderImagesList() {
    return (
      this.props.birdList.map((birdItem) => {
        return <li key={birdItem.title} onClick={() => {
          this.props.BirdDetailAction(birdItem)
        }}>
          <figure>
            <img id={birdItem.id} src={`../src/resources/images/${birdItem.title}.jpg`} alt={birdItem.title} title={birdItem.title} />
            <figcaption>{birdItem.title}</figcaption>
          </figure>

        </li>
      })
    );
  }

  renderImageDetail() {
    if (!(this.props.selectedBirdDetails)) {
      return (
        <BirdDetail bird={this.props.birdList[0]} />
      )
    }
    return (
      <BirdDetail bird={this.props.selectedBirdDetails} />
    )
  }

  render() {
    return (
      <div className="container app-gallery">
        <div className="row">
          <div className="img-list col-md-3">
            <h3> Gallery </h3>
            <ul>{this.renderImagesList()} </ul>
          </div>
          <div className="img-detail col-md-9">
            {this.renderImageDetail()}
          </div>
        </div>
      </div>
    )
  }
}
function mapStateToProps({ birdList, selectedBirdDetails }) {
  return { birdList, selectedBirdDetails };
}

export default connect(mapStateToProps, { BirdDetailAction })(Gallery);