import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, NavLink, withRouter } from 'react-router-dom';

export class Menubar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return this.props.menuItems.map((menuItem) => {
      const className = `nav-item`;
      return (
        <li
          className={className}
          key={menuItem.title}>
            <NavLink to={`/${menuItem.path}`} exact activeClassName="nav-active"
              className="nav-link"> {menuItem.title} </NavLink>
        </li>
      )
    })
  };
}

class Header extends Component {
  render() {
    return (
      <header className="app-header">
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
          <Link className="navbar-brand" to="/">
            <img className="logo d-inline-block align-top" src="../src/resources/images/bird-logo.jpg" alt="Bird Life logo" />
            Bird Life
            </Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav"><Menubar { ...this.props } /></ul>
          </div>
        </nav>
      </header>
    )
  }
}

function mapStatetoProps({ menuItems }) {
  return { menuItems }
}

export default withRouter(connect(mapStatetoProps)(Header));
