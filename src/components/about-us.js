import React from 'react';

export default function () {
  return (
      <div className="app-about-us container">
        <h3> About Us </h3>
        <h4> Our Vision </h4>
        <p>
          Bird Life wishes to see a world where nature and people live in greater harmony,
          more equitably and sustainably.
        </p>
        <h4> Our Mission </h4>
        <p>
          The Bird Life strives to conserve birds,
          their habitats and global biodiversity, working with people towards
          sustainability in the use of natural resources.
        </p>
        <h4> Our Commitment </h4>
        <ul>
          <li> To prevent extinction in the wild.</li>
          <li> To maintain and where possible improve the conservation status of all bird species.</li>
          <li> To conserve the sites and habitats important for birds and other biodiversity.</li>
          <li> To sustain the vital ecological systems that underpin human livelihoods, and enrich the quality of
            people's lives.
          </li>
        </ul>
      </div>
  );
}