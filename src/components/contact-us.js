import React from 'react';
import { Provider as AlertProvider } from 'react-alert';
import AlertTemplate  from 'react-alert-template-basic';
import Contact from '../containers/contact';

const options = {
    timeout: 3000,
    offset: '50px',
    position: 'top right'
};

export default function () {
    return (
        <AlertProvider template={AlertTemplate} {...options}>
            <Contact />
        </AlertProvider>
    )
}
