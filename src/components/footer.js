import React from 'react';

export default function () {
  return (
    <footer className="app-footer">
      <div>Copyright &copy; Bird Life, 2018</div>
    </footer>
  );
}