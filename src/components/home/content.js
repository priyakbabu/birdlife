import React from 'react';

export default function () {
  return (
      <div className="app-home-content">
        <p>
          <img src="../../src/resources/images/bird5.jpg" alt="Macaw" title="Macaw"/>
          Birds are truly beautiful creatures, they are also fascinating and all around us,
          roosting and nesting on our buildings and feeding in our gardens and refuse dumps,
          they are easy to find and fun to observe. Birds are probably the most beloved group of
          wild animals on the planet. Their ubiquitous presence, colorful form, intelligent actions
          and cheeky mannerisms endear them to us all. Birds are easy to love.
        </p>

      </div>
  )
}