import React from 'react';

export default function () {
  return (
      <div className="app-home-banner">
        <div id="banner" className="carousel slide" data-ride="carousel">
          <ol className="carousel-indicators">
            <li data-target="#banner" data-slide-to="0" className="active"></li>
            <li data-target="#banner" data-slide-to="1"></li>
            <li data-target="#banner" data-slide-to="2"></li>
            <li data-target="#banner" data-slide-to="3"></li>
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img className="w-50" src="../src/resources/images/Top Oriole.jpg" alt="logo"/>
            </div>
            <div className="carousel-item">
              <img className=" w-50" src="../src/resources/images/Kingfisher.jpg" alt="logo"/>
            </div>
            <div className="carousel-item">
              <img className="w-50" src="../src/resources/images/Piping Plover.jpg" alt="logo"/>
            </div>
            <div className="carousel-item">
              <img className="w-50" src="../src/resources/images/Eagle.jpg" alt="logo"/>
            </div>
          </div>
          <a className="carousel-control-prev" href="#banner" data-slide="prev">
            <span className="carousel-control-prev-icon"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#banner" data-slide="next">
            <span className="carousel-control-next-icon"></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
      </div>
  );
}