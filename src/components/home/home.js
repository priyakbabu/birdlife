import React, { Component } from 'react';
import Content from './content';
import Banner from './banner';

export default class Home extends Component { 
  render() {
    return (
        <div className="container app-home">
          <Banner/>
          <Content/>
        </div>
    );
  }
}
