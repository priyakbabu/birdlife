import axios from 'axios';

export const sendMail =  function(formData) {
   const response = axios.post('http://localhost:3000/mailgun', formData);    
    return {
        type: 'SEND_EMAIL',
        payload: response
    }
}

export const resetState = function() {
    return {
        type: 'RESET'
    }
}

