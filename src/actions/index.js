export default function(birdItem) {
    return {
        type: 'BIRD_ITEM',
        payload: birdItem
    }
}