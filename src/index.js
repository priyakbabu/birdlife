import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import promise from 'redux-promise';

import Home from './components/home/home';
import Gallery from './containers/gallery';
import ContactUs from './components/contact-us';
import reducers from './reducers';
import Footer from './components/footer';
import Header from './containers/header';
import AboutUs from './components/about-us';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <div>
      <Router>
        <div className="app-root">
          <Header />
          <div className="app-body">
            <Switch>
              <Route path="/about-us" component={AboutUs} />
              <Route path="/contact-us" component={ContactUs} />
              <Route path="/gallery" component={Gallery} />
              <Route path="/" component={Home} />
            </Switch>
          </div>
          <Footer />
        </div>
      </Router>
    </div>
  </Provider>
  , document.querySelector('.app-container'));
