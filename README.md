# Project - Bird Life overview

Theme - Birds

## Framework and Testing

I have developed a single page application using React/Redux libraries to make highly dynamic, responsive and single responsibility components. 
I have written unit test cases for the components using Mocha and Chai. 

## Getting Started

### Pre-requisite

* Node.js - v8.0+

### Installation steps

```
git clone https://bitbucket.org/priyakbabu/birdlife.git  
cd birdlife  
npm install  
npm run start (for starting the project)  
npm run test (for executing test cases)  
```

### Project URL
```http://localhost:8080```
  
To run on a different port, update port number in **webpack.config.js**

### Technologies used
* HTML 5
* CSS
* Javascript
* React v16
* Redux 
* Redux form
* React router
* Bootstrap
### Testing frameworks
* Mocha
* Chai
* Enzyme
