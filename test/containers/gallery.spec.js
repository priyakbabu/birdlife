import { React, expect, createComponent } from '../test_helper';
import Gallery from '../../src/containers/gallery';
import BirdList from '../../src/reducers/reducer-bird-list';

describe('<Gallery />', () => {
  let component;
  let noOfBirdsImg = BirdList().length;

  beforeEach(() => {
    const props = {};
    component = createComponent(Gallery, props);
  });

  it('has four static image components', () => {
    expect(component.find('.img-list img')).to.have.length(noOfBirdsImg);
  })
  it('show the decription and image on click', () => {
    let values = ['Orioles','King','Piping','Eagle'];
    for (let i = 1; i <= noOfBirdsImg; i++){       
    const imageEle = component.find('#img'+i);
    imageEle.simulate('click');
    const someElement = component.find('.img-detail');
    expect(someElement).to.exist;
    expect(someElement.text()).to.contain(values[i-1]);
    expect(someElement.find('img'));
    }
  })  
});