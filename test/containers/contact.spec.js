import { React, shallow, expect, createComponent, createComponentwithAlert } from '../test_helper';
import ConnectedContact, { TextField, TextAreaField } from '../../src/containers/contact';

describe('<Contact />', () => {
  describe('contact render', () => {
    let component;
    beforeEach(() => {
      const props = {};
      component = createComponentwithAlert(ConnectedContact, props);
    });

    it("shows help text when first name is set to blank", () => {
      const input = component.find('input').first();
      input.simulate('blur');
      const someElement = component.find('.text-help').first();
      expect(someElement).to.exist;
      expect(someElement.text()).to.equal('Enter a name');
    });

  });

  describe('TextField', () => {
    context("when in an error state", () => {
      it("renders an error message for the name", () => {
        const component = formTextFieldSetup('name', '', true, 'Enter a name', 'Enter your name');
        assertErrorMessage(component, 'Enter a name');
      });

      it("renders an error message for the email", () => {
        const component = formTextFieldSetup('email', '', true, 'Enter a valid Email', 'Enter your e-mail address');
        assertErrorMessage(component, 'Enter a valid Email');
      });

      it("renders an error message for the website", () => {
        const component = formTextFieldSetup('website', '', true, 'Enter a website', 'Enter your website');
        assertErrorMessage(component, 'Enter a website');
      });
    });

    context("when successful", () => {
      it("name field should have correct value", () => {
        const component = formTextFieldSetup('name', 'Test name', true, '', 'Enter your name');
        assertSuccessful(component, 'name', 'Test name');
      });

      it("email field should have correct value", () => {
        const component = formTextFieldSetup('email', 'test@test.com', true, '', 'Enter your e-mail address');
        assertSuccessful(component, 'email', 'test@test.com');
      });

      it("website field should have correct value", () => {
        const component = formTextFieldSetup('website', 'www.google.com', true, '', 'Enter your website');
        assertSuccessful(component, 'website', 'www.google.com');
      });
    });
  });

  describe('TextAreaField', () => {
    context("when in an error state", () => {
      it("renders an error message for the message", () => {
        const component = formTextFieldSetup('message', '', true, 'Enter a message', 'Enter your message');
        assertErrorMessage(component, 'Enter a message');
      });
    });

    context("when successful", () => {
      it("message field should have correct value", () => {
        const component = formTextAreaFieldSetup('message', 'Test message', true, '', 'Enter your message');
        assertSuccessful(component, 'message', 'Test message');
      });
    });
  });

  function formTextFieldSetup(name, value, touched, error, placeholder) {
    const field = {
      input: { name: name, value: value, readOnly: true },
      meta: { touched: touched, error: error },
      placeholder: placeholder
    };
    return shallow(<TextField { ...field }/>);
  }

  function formTextAreaFieldSetup(name, value, touched, error, placeholder) {
    const field = {
      input: { name: name, value: value, readOnly: true },
      meta: { touched: touched, error: error },
      placeholder: placeholder
    };
    return shallow(<TextAreaField { ...field }/>);
  }

  function assertErrorMessage(component, errorMessage) {
    const elemHelpBlock = component.find('.text-help').first();
    expect(elemHelpBlock).to.exist;
    expect(elemHelpBlock.text()).to.equal(errorMessage);
  }

  function assertSuccessful(component, elementName, message) {
    const elemHelpBlock = component.find('.text-help').first();
    expect(elemHelpBlock).to.exist;
    expect(elemHelpBlock.text()).to.be.empty;
    expect(component.find('[name="' + elementName + '"]').first().html()).to.contain(message);
  }

});

