import { React, createRouterComponent, expect } from '../test_helper';
import { Menubar } from '../../src/containers/header';
import menuItems from '../../src/reducers/reducer-menu-items';

describe('<Header />', () => {
  describe('<Menubar />', () => {
    let component;
    const props = {
      menuItems: menuItems()
    };

    beforeEach(() => {
      component = createRouterComponent(Menubar, props)
    });

    it('should have rendered menu items', () => {
      expect(component.find('li')).to.have.length(4);
    });
  });
 
});