import { React, shallow, expect } from '../test_helper';
import AboutUs from '../../src/components/about-us';

describe('<About Us />', () => {
  let component;

  beforeEach(() => {
    component = shallow(<AboutUs/>);
  });

  it('has the app-about-us class', () => {
    expect(component.find('div')).to.have.className('app-about-us');
  });

  it('has the About Us heading', () => {
    expect(component.find('h3').text()).to.equal(' About Us ');
  });

});