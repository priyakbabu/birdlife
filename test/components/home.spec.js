import { React, expect, createComponent } from '../test_helper';
import Home from '../../src/components/home/home';
import Banner from '../../src/components/home/banner';
import Content from '../../src/components/home/content';

describe('<Home />', () => {
  let component;

  beforeEach(() => {
    component = createComponent(Home);
  });
  it('has the banner element', () => {
    expect(component).to.containMatchingElement(<Banner />);
  });

  it('has the content element', () => {
    expect(component).to.containMatchingElement(<Content />);
  });
});